import { createSlice } from "@reduxjs/toolkit";

type ModalState = {
    active: boolean;
}

const initialState: ModalState = {
    active: false,
}

const modalSlice = createSlice({
    name: 'modal',
    initialState,
    reducers: {
        setModalActive: (state, action) => {
            state.active = action.payload.active
        }
    }
})

export default modalSlice.reducer
export const { setModalActive } = modalSlice.actions