import { createSlice, createAsyncThunk, PayloadAction, UnknownAction } from "@reduxjs/toolkit";

const API_URL = 'https://retoolapi.dev/JyE7rp/todos';

type Todo = {
    id: string;
    text: string;
    isDone: boolean;
    deadline: string;
}

type TodosState = {
    todos: Todo[];
    status: null | 'rejected' | 'loading' | 'resolved';
    error: string | null;
}

export const fetchTodos = createAsyncThunk<Todo[], void, {rejectValue: string}>(
    'todos/fetchTodos',
    async function(_, {rejectWithValue}) {
        const response = await fetch(API_URL);

        if (!response.ok) {
            return rejectWithValue('Could not fetch todos');
        }

        const data = await response.json();

        return data;
    }
);

export const switchStatusTodo = createAsyncThunk<Todo, string, {rejectValue: string, state: {todos: TodosState}}>(
    'todos/switchStatusTodo',
    async function(id, {rejectWithValue, getState}) {
        const todo = getState().todos.todos.find(todo => todo.id === id);

        if (todo) {
            const response = await fetch(API_URL + `/${id}`, {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    isDone: !todo.isDone,
                }),
            })

            if (!response.ok) {
                return rejectWithValue('Could not switch todo`s status');
            }

            return (await response.json());
        }

        return rejectWithValue('Could not find todo');
    }
);

export const addNewTodo = createAsyncThunk<Todo, {inputText: string, inputDate: string}, {rejectValue: string}>(
    'todos/addNewTodo',
    async function({inputText, inputDate}, {rejectWithValue}) {

        const todo = {
            text: inputText,
            isDone: false,
            deadline: inputDate,
        };

        const response = await fetch(API_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(todo),
        });

        if (!response.ok) {
            return rejectWithValue('Could not add task');
        }

        const data = await response.json();
        return data;
    }
);

export const deleteTodo = createAsyncThunk<string, string, { rejectValue: string }>(
    'todos/deleteTodo',
    async function(id, {rejectWithValue}) {
        const response = await fetch(API_URL + `/${id}`, {
            method: 'DELETE',
        })

        if (!response.ok) {
            return rejectWithValue('Could not delete task');
        }

        return id;
    }
);

const initialState: TodosState = {
    todos: [],
    status: null,
    error: null,
}

const todoSlice = createSlice({
    name: 'todos',
    initialState,
    selectors: {
        selectTodosStore: (state) => state,
        selectTodos: (state) => state.todos,
    },
    reducers: {
        addTask(state, action: PayloadAction<Todo>){
            state.todos.push(action.payload)
        },
        toggleTodo(state, action: PayloadAction<{ id: string }>){

            const updateTodos = state.todos.map((todo) => {
                if (todo.id === action.payload.id) {
                    let isDone2 = todo.isDone === true ? false : true;
                    return { ...todo, isDone: isDone2}
                } else {
                    return todo
                }
            })
            state.todos = updateTodos
        },
        deleteTask(state, action: PayloadAction<{ id: string}>) {
            state.todos = state.todos.filter(todo => todo.id !== action.payload.id)
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchTodos.pending, (state) => {
                state.status = 'loading';
                state.error = null;
            })
            .addCase(fetchTodos.fulfilled, (state, action) => {
                state.status = 'resolved';
                state.todos = action.payload;
            })
            .addCase(switchStatusTodo.pending, (state, action) => {
                state.status = 'loading';
                state.error = action.payload ?? null;
            })
            .addCase(switchStatusTodo.fulfilled, (state, action) => {
                const updateTodos = state.todos.map((todo) => {
                    if (todo.id === action.payload.id) {
                        let isDone2 = todo.isDone === true ? false : true;
                        return { ...todo, isDone: isDone2}
                    } else {
                        return todo
                    }
                })
                state.todos = updateTodos;
                state.status = 'resolved';
            })
            .addCase(addNewTodo.pending, (state) => {
                state.status = 'loading';
                state.error = null;
            })
            .addCase(addNewTodo.fulfilled, (state, action) => {
                state.todos.push(action.payload);
                state.status = 'resolved';
            })
            .addCase(deleteTodo.fulfilled, (state, action) => {
                state.todos = state.todos.filter(todo => todo.id !== action.payload)
            })
            .addMatcher(isError, (state, action: PayloadAction<string>) => {
                state.status = 'rejected';
                state.error = action.payload;
            })
    },
});

export const {addTask, toggleTodo, deleteTask} = todoSlice.actions;
export const {selectTodosStore, selectTodos} = todoSlice.selectors;
export default todoSlice.reducer;

function isError(action: UnknownAction) {
    return action.type.endsWith('rejected');
}