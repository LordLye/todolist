import { ReactNode } from 'react';
import styled from 'styled-components';
import React from 'react';

const Wrapper = styled.main`
    padding: 0 1.5rem;

    @media(min-width: 767px) {
        padding: 0rem 3rem;
    }
`;

type MainProps = {
    children: ReactNode;
}

export const Main: React.FC<MainProps> = ({children}) => {
    return (
        <Wrapper>
            {children}
        </Wrapper>
    )
}
