import styled from 'styled-components'

export const Ul = styled.ul`
    margin: 0;
    padding: 0 0.5rem;
    display: flex;
    flex-direction: column;
    gap: 5px
`
