import styled from 'styled-components'

export const Button = styled.button`
    width: 100px;
    min-height: 30px;
    border-radius: 5px;
    border: none;
    background-color: LightSalmon;
    font-weight: var(--fw-normal);
    cursor: pointer;
`;