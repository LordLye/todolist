import {Button} from '../styled/Button'
import { useAppDispatch } from '../../hook.ts'
import { setModalActive } from '../../store/modalSlice.ts'
import React from 'react';

export const OpenModalButton: React.FC = () => {
  const dispatch = useAppDispatch()

  return (
    <Button onClick={() => dispatch(setModalActive({active : true}))}>Add task</Button>
  )
}
