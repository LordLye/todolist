import styled from 'styled-components'
import { useAppDispatch } from '../../hook.ts';
import { switchStatusTodo, deleteTodo } from '../../store/todoSlice.ts';
import { ImCross } from "react-icons/im";
import React from 'react';

const Li = styled.li`
    list-style: none;
    display: flex;
    gap: 5px;
    cursor: pointer;
    display: flex;
    align-items: center;
`
interface TaskLabelProps {
    htmlFor: string;
    $isDone: boolean;
    children: React.ReactNode;
}

const TaskLabel = styled.label<TaskLabelProps>`
    font-size: 18px;
    text-decoration: ${(props) => props.$isDone ? "line-through" : "none"};
    margin-right: 5px;

    &:hover {
        background: #eee;
    }
`

const ButtonDelete = styled.button`
    border: none;
    background: none;
    cursor: pointer;
    color: red;
    width: 25px;
    height: 25px;
    display: flex;
    align-items: center;
    justify-content: center;

    &:hover {
        background: #eee;
    }

    &:active {
        opacity: 0.5;
    }
`

export interface TodoItemProps {
    id: any;
    text: string;
    isDone: boolean;
    deadline: string;
};

/* type TodoItemProps = StartTodoItem & TodoItemProp; */



export const TodoItem: React.FC<TodoItemProps> = ({ id, text, isDone, deadline }) => {
    const dispatch = useAppDispatch()

    const handlToggleTodo = () => {
        dispatch(switchStatusTodo(id))
    }

    const handlDeleteTodo = () => {
        dispatch(deleteTodo(id))
    }


    const calcDaysLeft = (days) => {

        if (days > 0 && days < 1) {
            return 1;
        }

        if (days < 0 && days > -1) {
            return 0;
        }

        return Math.trunc(days);
    }



    const daysLeft = calcDaysLeft((new Date(deadline).valueOf() - new Date().valueOf()) / (60 * 60 * 24 * 1000));

    return (
        <Li>
            <input
                type="checkbox"
                name="task-item"
                id={id}
                checked={isDone}
                onChange={handlToggleTodo} 
            />
            <TaskLabel
                htmlFor={id}
                $isDone={isDone}
            >
                {text} (left {daysLeft} days)
            </TaskLabel>
            <ButtonDelete onClick={handlDeleteTodo}>
                <ImCross/>
            </ButtonDelete>
        </Li>
    );
};
