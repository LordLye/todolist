import { TodoItem, TodoItemProps } from './TodoItem.tsx'
import { Title } from '../styled/Title.jsx'
import { Ul } from '../styled/Ul.jsx'
import React from 'react';

interface TodoListProps {
    title: string;
    items: TodoItemProps[];
}

export const TodoList: React.FC<TodoListProps> = ({ title, items }) => {

    return (
        <>
            <Title as={'h2'}>{title}</Title>
            <Ul>
                {
                    items.map(todo => (
                        <TodoItem
                            key={todo.id}
                            {...todo}
                        />
                    ))
                }
            </Ul>
        </>
    )
}
