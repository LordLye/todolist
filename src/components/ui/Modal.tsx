import React, { ReactNode } from "react";
import styled from "styled-components";
import { useDispatch } from "react-redux";
import { setModalActive } from "../../store/modalSlice.ts";

const Block = styled.div`
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background-color: rgba(0,0,0,0.4);
    display: flex;
    align-items: center;
    justify-content: center;
    transform: scale(1);
`;

const Content = styled.div`
    width: 800px;
    height: 200px;
    background-color: white;
    border-radius: 12px;
`;

type ModalProps = {
    children: ReactNode;
}

export const Modal: React.FC<ModalProps> = ({ children }) => {
    const dispatch = useDispatch()

    const keydownHandler = (e: KeyboardEvent) => {
        if (e.key==='Escape') {
            dispatch(setModalActive({active: false}))
        }
    }

    React.useEffect(() => {
        document.addEventListener('keydown', keydownHandler);
        return () => document.removeEventListener('keydown', keydownHandler);
    });


    return (
        <Block onClick={() => dispatch(setModalActive({active: false}))}>
            <Content onClick={(e) => e.stopPropagation()}>{children}</Content>
        </Block>
    )
};
