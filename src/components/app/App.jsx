import { TodoList } from '../ui/TodoList.tsx'
import { Title } from '../styled/Title.jsx'
import { OpenModalButton } from '../ui/OpenModalButton.tsx'
import { Modal } from '../ui/Modal.tsx'
import { ContentAddTask } from '../blocks/ContentAddTask.tsx'
import { Main } from '../layout/Main.tsx'
import { ThemeProvider, createGlobalStyle } from 'styled-components'
import { useAppDispatch, useAppSelector } from '../../hook.ts'
import { useEffect } from 'react'
import { fetchTodos, selectTodos, selectTodosStore } from '../../store/todoSlice.ts'
import React from 'react';

const GlobalStyle = createGlobalStyle`
  * {
    padding: 0;
  }
`

const theme = {}

export default function App() {
  const dispatch = useAppDispatch()

  const todos = useAppSelector(selectTodos)
  const modalActive = useAppSelector((state) => state.modal.active)

  const {status, error} = useAppSelector(selectTodosStore)

  const getOverdueTodo = () => {
    const today = new Date();
    return todos.filter((todo) => !todo.isDone && new Date(todo.deadline) < today)
  }

  const getActualTodo = () => {
    const today = new Date();
    return todos.filter((todo) => !todo.isDone && new Date(todo.deadline) >= today)
  }

  const getCompletedTodo = () => {
    return todos.filter((todo) => todo.isDone)
  }

  useEffect(() => {
    dispatch(fetchTodos());
  }, [dispatch]);

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <Main>
        <Title>Todo List</Title>
        <OpenModalButton/>

        {status === 'loading' && <h2>Loading...</h2>}
        {error && <h2>An error occed: {error}</h2>}

        <TodoList 
          title= 'Overdue'
          items = {getOverdueTodo()}
        />
        <TodoList 
          title= 'Actual'
          items = {getActualTodo()}
        />
        <TodoList 
          title= 'Completed'
          items = {getCompletedTodo()}
        />
        {modalActive && 
        <Modal>
          <ContentAddTask />
        </Modal>}
      </Main>
    </ThemeProvider>
  );
}
