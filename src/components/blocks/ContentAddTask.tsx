import styled from 'styled-components'
import { useState } from 'react'
import { Button } from '../styled/Button.jsx'
import { useAppDispatch } from '../../hook.ts'
import { addNewTodo } from '../../store/todoSlice.ts'
import React from 'react';

const Container = styled.div`
    padding: 15px;
    display: flex;
    flex-direction: column;
    gap: 5px;
`

const Item = styled.div`
    display: flex;
    gap: 7px;
`

const Input = styled.input``

const Label = styled.label``
    

export const ContentAddTask: React.FC = () => {
    const dispatch = useAppDispatch()

    const [inputText, setInputText] = useState('')
    const [inputDate, setInputDate] = useState('')

    return (
        <Container>
            <Item>
                <Label htmlFor="task-name">Task name</Label>
                <Input 
                    type="text" 
                    name="task-name" 
                    id="task-name" 
                    value={inputText} 
                    onChange={(e) => setInputText(e.target.value)} />
            </Item>
            <Item>
                <Label htmlFor="task-deadline">Task deadline</Label>
                <Input 
                    type="date" 
                    name="task-deadline" 
                    id="task-deadline" 
                    value={inputDate} 
                    onChange={(e) => setInputDate(e.target.value)} />
            </Item>
            <Button type="submit" onClick={() => {
                    dispatch(addNewTodo({inputText, inputDate}))
                    setInputText('')
                    setInputDate('')
                }
            }>Add Task</Button>
        </Container>
    )
}
