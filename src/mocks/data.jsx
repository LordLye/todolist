/* export type StartTodoItem = {
    id: number;
    text: string;
    isDone: boolean;
    deadline: string;
} */

/* export type StartTodoList = StartTodoItem[]; */

export const startTodoList = [
    { id: 1, text: 'Buy groceries', isDone: true, deadline: '2026-07-10'},
    { id: 2, text: 'Do laundry', isDone: false, deadline: '2024-06-10'},
    { id: 3, text: 'Clean bathroom', isDone: true, deadline: '2026-07-10'},
    { id: 4, text: 'Walk with orangutan', isDone: false, deadline: '2026-07-10'},
    { id: 5, text: 'Call mom', isDone: false, deadline: '2024-06-10'},
    { id: 6, text: 'Pay bills', isDone: false, deadline: '2026-07-10'},
    { id: 7, text: 'Buy birthday gift', isDone: false, deadline: '2026-07-10'},
    { id: 8, text: 'Finish project', isDone: false, deadline: '2026-07-10'},
    { id: 9, text: 'Go to gym', isDone: false, deadline: '2026-07-10'},
    { id: 10, text: 'Read book', isDone: false, deadline: '2026-07-10'},
];
