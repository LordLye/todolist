import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './components/app/App.jsx';
import { Provider } from 'react-redux';
import store, { persistor } from './store/index.ts';
import { PersistGate } from 'redux-persist/integration/react'

const rootElement = document.getElementById('root');

if (rootElement) {
  const root = ReactDOM.createRoot(rootElement);
  root.render(
    <React.StrictMode>
      <Provider store={ store }>
        <PersistGate loading={ null } persistor={ persistor }>
          <App />
        </PersistGate>
      </Provider>
    </React.StrictMode>
  );
} else {
  console.error('Failed to find element with id "root"');
}


